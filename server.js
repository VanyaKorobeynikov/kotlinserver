const jwt = require('jsonwebtoken'),
      crypto = require('crypto');
import './models/User';
import mongoose from 'mongoose';

const User = mongoose.model('User');

function generateToken(user) {
  return jwt.sign(user, 'secret', {
    expiresIn: 604800 // in seconds
  });
}

// Set user info from request
function setUserInfo(request) {
  return {
    _id: request._id,
    email: request.email
  };
}

export function login(req, res, next) {
  const userInfo = setUserInfo(req.user);

  res.status(200).json({
    token: `JWT ${ generateToken(userInfo) }`,
    user: userInfo
  });
};

//========================================
// Registration Route
//========================================
export function register(req, res, next) {
  // Check for registration errors
  const email = req.body.email;
  const password = req.body.password;

  // Return error if no email provided
  if (!email) {
    return res.status(422).send({ error: 'You must enter an email address.' });
  }

  // Return error if no password provided
  if (!password) {
    return res.status(422).send({ error: 'You must enter a password.' });
  }

  User.findOne({ email: email }, function (err, existingUser) {
    if (err) {
      return next(err);
    }

    // If user is not unique, return error
    if (existingUser) {
      return res.status(422).send({ error: 'That email address is already in use.' });
    }

    // If email is unique and password was provided, create account
    let user = new User({
      email: email,
      password: password
    });

    user.save(function (err, user) {
      if (err) {
        return next(err);
      }

      // Subscribe member to Mailchimp list
      // mailchimp.subscribeToNewsletter(user.email);

      // Respond with JWT if user was created

      let userInfo = setUserInfo(user);

      res.status(201).json({
        token: 'JWT ' + generateToken(userInfo),
        user: userInfo
      });
    });
  });
}
import mongoose from 'mongoose';
const Schema = mongoose.Schema;

var FinisherSchema = new Schema({
    name: { type: String, required: true },
    photo: { type: Schema.Types.Mixed },
    created: { type: Date },
    updated: { type: Date },
    city: { type: String },
    country: { type: String },
    age: { type: Number },
    sport: { type: String },
    sex: { type: String },
    about: { type: String },
    starts: { type: Array, "default": [], ref: 'Start' }

});

const Finisher = mongoose.model('Finisher', FinisherSchema);
import mongoose from 'mongoose';
const Schema = mongoose.Schema;

var PhotoSchema = new Schema({
    name: { type: String, required: true },
    path: { type: String, required: true },
    created: { type: Date },
    updated: { type: Date },
    year: { type: Number },
    finisher: { type: Schema.Types.ObjectId, ref: 'Finisher' },
    destination: { type: String },
    caption: { type: String },
    start: { type: Schema.Types.ObjectId, ref: 'Start' }
});

const Photo = mongoose.model('Photo', PhotoSchema);
import mongoose from 'mongoose';
const Schema = mongoose.Schema;

var RequestSchema = new Schema({
    name: { type: String },
    comment: { type: String },
    created: { type: Date },
    updated: { type: Date },
    country: { type: String },
    contact: { type: String },
    age: { type: String },
    date: { type: String },
    type: { type: String }
});

const Request = mongoose.model('Request', RequestSchema);
import mongoose from 'mongoose';
const Schema = mongoose.Schema;

var StartSchema = new Schema({
    name: { type: String },
    about: { type: String },
    created: { type: Date },
    updated: { type: Date },
    date: { type: Date },
    time: { type: String },
    wheather: { type: String },
    food: { type: String },
    type: { type: String },
    finishers: [{
        type: Schema.Types.ObjectId, ref: 'Finisher'
    }],
    photos: [{
        type: Schema.Types.ObjectId, ref: 'Photo'
    }]
});

const Start = mongoose.model('Start', StartSchema);
import mongoose from 'mongoose';
import bcrypt from 'bcrypt-nodejs';

const Schema = mongoose.Schema;

const UserSchema = new Schema({
  email: {
    type: String,
    lowercase: true,
    unique: true,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  resetPasswordToken: { type: String },
  resetPasswordExpires: { type: Date }
}, {
  timestamps: true
});
UserSchema.pre('save', function (next) {
  const user = this,
        SALT_FACTOR = 5;

  if (!user.isModified('password')) return next();

  bcrypt.genSalt(SALT_FACTOR, function (err, salt) {
    if (err) return next(err);

    bcrypt.hash(user.password, salt, null, function (err, hash) {
      if (err) return next(err);
      user.password = hash;
      next();
    });
  });
});
UserSchema.methods.comparePassword = function (candidatePassword, cb) {
  bcrypt.compare(candidatePassword, this.password, function (err, isMatch) {
    if (err) {
      return cb(err);
    }

    cb(null, isMatch);
  });
};

const User = mongoose.model('User', UserSchema);
const passport = require('passport'),
      JwtStrategy = require('passport-jwt').Strategy,
      ExtractJwt = require('passport-jwt').ExtractJwt,
      LocalStrategy = require('passport-local');
import mongoose from 'mongoose';
import './models/User';
const User = mongoose.model('User');

const localOptions = { usernameField: 'email' };
const localLogin = new LocalStrategy(localOptions, function (email, password, done) {
  User.findOne({ email: email }, function (err, user) {
    if (err) {
      return done(err);
    }
    if (!user) {
      return done(null, false, { error: 'Your login details could not be verified. Please try again.' });
    }

    user.comparePassword(password, function (err, isMatch) {
      if (err) {
        return done(err);
      }
      if (!isMatch) {
        return done(null, false, { error: "Your login details could not be verified. Please try again." });
      }

      return done(null, user);
    });
  });
});
const jwtOptions = {
  // Telling Passport to check authorization headers for JWT
  jwtFromRequest: ExtractJwt.fromAuthHeader(),
  // Telling Passport where to find the secret
  secretOrKey: 'secret'
};
const jwtLogin = new JwtStrategy(jwtOptions, function (payload, done) {
  User.findById(payload._id, function (err, user) {
    if (err) {
      return done(err, false);
    }

    if (user) {
      done(null, user);
    } else {
      done(null, false);
    }
  });
});
passport.use(jwtLogin);
passport.use(localLogin);
import passportService from './pass';
import express from 'express';
import passport from 'passport';
import { register, login } from './auth';

const requireAuth = passport.authenticate('jwt', { session: false });
const requireLogin = passport.authenticate('local', { session: false });

export default function (app) {
  // Initializing route groups
  const apiRoutes = express.Router(),
        authRoutes = express.Router();

  //=========================
  // Auth Routes
  //=========================

  // Set auth routes as subgroup/middleware to apiRoutes
  apiRoutes.use('/auth', authRoutes);

  // Registration route
  authRoutes.post('/register', register);

  // Login route
  authRoutes.post('/login', requireLogin, login);

  // Set url for API group routes
  app.use('/api', apiRoutes);
};
import express from 'express';
import bodyParser from 'body-parser';
import * as db from './utils/dbutils';
import cors from 'cors';
import multer from 'multer';
import * as fs from 'fs';
import mime from 'mime';
import router from './router';
import AuthCtrl from './auth';
import { register, login } from './auth';
import passportService from './pass';
import passport from 'passport';

const requireAuth = passport.authenticate('jwt', { session: false });
const requireLogin = passport.authenticate('local', { session: false });

const apiRoutes = express.Router(),
      authRoutes = express.Router();

//=========================
// Auth Routes
//=========================

// Set auth routes as subgroup/middleware to apiRoutes
apiRoutes.use('/auth', authRoutes);

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, '../kotlin/images/gallery');
  },
  filename: (req, file, cb) => {
    return cb(null, Date.now() + '.' + mime.extension(file.mimetype));
  }
});
const finishersPhoto = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, '../kotlin/images/finishers');
  },
  filename: (req, file, cb) => {
    const name = req.body.name.replace(/ /g, "_").toLowerCase();
    return cb(null, name + '.' + mime.extension(file.mimetype));
  }
});
const finisherPhotoUpload = multer({ storage: finishersPhoto }).single('finisherPhoto');
const upload = multer({ storage: storage }).single('photo');

const app = express();

db.setUpConnection();
const urlEncoded = bodyParser.urlencoded({ extended: false });
const jsonParse = bodyParser.json();
app.use(cors({ origin: '*' }));
//app.use(bodyParser.urlencoded({extended: false}));
app.post('/api/login', urlEncoded, requireLogin, login);
app.get('/api/finishers', (req, res) => {
  db.getFinishers().then(data => res.send(data));
});
app.get('/api/finisher/:id', (req, res) => {
  db.getFinisher(req.params.id).then(data => res.send(data));
});
app.get('/api/finisher/:id/delete', requireAuth, (req, res) => {
  db.deleteFinisher(req.params.id).then(data => res.send(data));
});

app.get('/api/photos/:year', (req, res) => {
  db.getAllPhotos(req.params.year).then(data => res.send(data));
});
app.get('/api/photo/:id', (req, res) => {
  db.getPhoto(req.params.id).then(data => res.send(data));
});

app.get('/api/starts/:type', (req, res) => {

  db.getStarts(req.params.type).then(data => res.send(data));
});

app.get('/api/start/:id', (req, res) => {
  db.getStart(req.params.id).then(data => res.send(data));
});

app.get('/api/start/:id/delete', requireAuth, (req, res) => {
  db.deleteStart(req.params.id).then(data => res.send(data));
});
app.get('/api/requests', (req, res) => {
  db.getRequests().then(data => res.send(data));
});

app.get('/api/request/:id/delete', requireAuth, (req, res) => {
  db.deleteRequest(req.params.id).then(data => res.send(data));
});

app.post('/api/finisher/add', requireAuth, (req, res) => {
  finisherPhotoUpload(req, res, err => {
    const path = req.file ? req.file.filename : '';
    db.addFinisher(req.body, path).then(data => res.send(data)).catch(err => console.log(err));
    if (err) {
      return res.end("Error uploading file.");
    }
  });
});

app.post('/api/finisher/edit', requireAuth, (req, res) => {
  finisherPhotoUpload(req, res, err => {
    if (err) {
      return res.end("Error uploading file.");
    }
    db.getFinisher(req.body._id).then(data => {
      const path = req.file ? req.file.filename : data.photo;
      db.editFinisher(req.body, path).then(data => res.send(data)).catch(err => console.log(err));
    }).catch(err => console.log(err));
  });
});

app.post('/api/start/edit', jsonParse, requireAuth, (req, res) => {
  db.editStart(req.body).then(data => res.send(data));
});

app.post('/api/photo/add', requireAuth, (req, res) => {
  upload(req, res, err => {
    db.addPhoto(req.body, req.file).then(data => res.send(data)).catch(err => console.log(err));
    if (err) {
      return console.log("upload error");
    }
  });
});

app.post('/api/start/add', requireAuth, jsonParse, (req, res) => {
  db.addStart(req.body).then(data => res.send(data)).catch(err => console.log(err));
});

app.post('/api/request/add', jsonParse, requireAuth, (req, res) => {
  db.addRequest(req.body).then(data => res.send(data)).catch(err => console.log(err));
});

app.post('/api/photo/edit', jsonParse, requireAuth, (req, res) => {
  db.editPhoto(req.body).then(data => res.send(data));
});
//app.post('/api/register', urlEncoded, register);


app.get('/api/photo/:id/delete', requireAuth, (req, res) => {
  db.getPhoto(req.params.id).then(data => {
    fs.unlink('../kotlinclient/public/images/' + data.name, err => {
      if (err) console.log(err);
      db.deletePhoto(data._id).then(() => res.send("Delete complete"));
    }).catch(err => console.log(err));
  });
});
const server = app.listen(3001, function () {
  console.log('Example app listening on port 3001!');
});
import mongoose from 'mongoose';
import '../models/Finisher';
import '../models/Photo';
import '../models/Start';
import '../models/Request';

const Finisher = mongoose.model('Finisher');
const Photo = mongoose.model('Photo');
const Start = mongoose.model('Start');
const Request = mongoose.model('Request');

export function setUpConnection() {
    console.log('DB connected');
    mongoose.connect('mongodb://localhost/kotlin');
}

export function addFinisher(data, path) {
    const finisher = new Finisher({
        name: data.name,
        city: data.city,
        photo: path,
        country: data.country,
        age: data.age ? +data.age : null,
        sex: data.sex,
        about: data.about,
        sport: data.sport,
        created: Date(),
        updated: Date(),
        starts: []

    });

    return finisher.save();
}

export function editFinisher(data, path) {
    return Finisher.update({ _id: data._id }, {
        name: data.name,
        city: data.city,
        country: data.country,
        photo: path,
        age: +data.age,
        sex: data.sex,
        about: data.about,
        sport: data.sport,
        updated: Date()
    });
}

export function editStart(data) {
    return Start.update({ _id: data._id }, {
        name: data.name,
        about: data.about,
        updated: Date(),
        date: data.date,
        time: data.time,
        wheather: data.wheather,
        food: data.food,
        type: data.type,
        finishers: data.finishers
    });
}

export function getFinishers() {
    return Finisher.find();
}
export function getStarts(type) {
    if (type == "undefined") return Start.find().populate('finishers').sort({ date: -1 }).exec((err, starts) => {
        if (err) console.log(err);
        return starts;
    });
    if (type == "solo") return Start.find().populate('finishers').sort({ date: -1 }).where({ type: "solo" }).exec((err, starts) => {
        if (err) console.log(err);
        return starts;
    });
    if (type == "relay") return Start.find().populate('finishers').sort({ date: 1 }).where({ type: "relay" }).exec((err, starts) => {
        if (err) console.log(err);
        return starts;
    });
}
export function getRequests() {
    return Request.find();
}

export function getFinisher(id) {
    return Finisher.findOne({ _id: id });
}

export function getStart(id) {
    return Start.findOne({ _id: id }).populate('photos').populate('finishers').exec((err, start) => {
        if (err) console.log(err);
        return start;
    });
}
export function deleteStart(id) {
    return Start.remove({ _id: id }, err => {
        if (err) return console.log(err);
        return "removed";
    });
}
export function deleteRequest(id) {
    return Request.remove({ _id: id }, err => {
        if (err) return console.log(err);
        return "removed";
    });
}
export function deleteFinisher(id) {
    return Finisher.remove({ _id: id }, err => {
        if (err) return console.log(err);
        return "removed";
    });
}
export function deletePhoto(id) {
    return Photo.remove({ _id: id }, err => {
        if (err) return console.log(err);
        return "removed";
    });
}
export function addStart(data) {
    const start = new Start({
        name: data.name,
        about: data.about,
        created: Date(),
        updated: Date(),
        date: data.date,
        time: data.time,
        wheather: data.wheather,
        food: data.food,
        type: data.type,
        finishers: data.finishers
    });

    return start.save();
}

export function addRequest(data) {
    const request = new Request({
        name: data.name,
        comment: data.comment,
        created: Date(),
        updated: Date(),
        date: data.date,
        contact: data.contact,
        country: data.country,
        age: data.age,
        type: data.type
    });

    return request.save();
}

export function editPhoto(data) {
    /* Photo.findByIdAndUpdate(data._id, (err, photo) => {
         if(err) console.log(err);
         photo.updated = Date();
         photo.year = data.year;
         photo.caption = data.caption;
         if(data.finisher && data.finisher != "undefined" && data.finisher != "noselect"){
             photo.set({finisher: data.finisher})
         }else{
             photo.unset({finisher: 1})
         }
     return photo.save();
     })*/
    /*Photo.find({}, function(err, photos){
        photos.forEach(function(element) {
        Start.update({_id: element.start}, {
            $push: {
                photos: element._id
            }
        }).exec();
    }, this);
    })
    
    
    return 'ok';*/
    /*Photo.findByIdAndUpdate(data._id, (err, photo) => {
        if(err) console.log(err);
        photo.updated = Date();
        photo.year = data.year;
        photo.caption = data.caption;
        if(data.finisher && data.finisher != "undefined" && data.finisher != "noselect"){
            photo.set({finisher: data.finisher})
        }else{
            photo.unset({finisher: 1})
        }
    return photo.save();
    })*/
    return Photo.update({ _id: data._id }, {
        $set: {
            updated: Date(),
            year: data.year,
            caption: data.caption
        }
    });
}

export function getAllPhotos(year) {
    if (year < 2000) {
        return Photo.find().populate('finisher').populate('start').exec((err, photo) => {
            if (err) console.log(err);
            return photo;
        });
    } else {
        return Photo.find({ year: year });
    }
}

export function getPhoto(id) {
    return Photo.findOne({ _id: id }).populate('finisher').exec((err, photo) => {
        return photo;
    });
}

export function addPhoto(data, file) {

    return Photo.create({
        name: file.filename,
        path: file.path,
        created: Date(),
        updated: Date(),
        year: data.year,
        destination: file.destination,
        caption: data.caption
    }, (err, photo) => {
        Start.update({ _id: data.start }, {
            $push: {
                photos: photo._id
            }
        }).exec();
        if (data.finisher && data.finisher != 'noselect' && data.finisher != 'undefined') {
            Photo.update({ _id: photo._id }, { $set: { finisher: data.finisher } }).exec();
        }
        if (data.start && data.start != 'noselect' && data.start != 'undefined') {
            Photo.update({ _id: photo._id }, { start: data.start }).exec();
        }
    });
    /*
    const photo = new Photo({
        name     : file.filename,
        path     : file.path,
        created  : Date(),
        updated  : Date(),
        year     : data.year,
    //        finisher : data.finisher || data.finisher == 'select finisher' ? data.finisher : null,
        destination: file.destination,
        caption  : data.caption,
    //        start    : data.start    
    });*/

    //return photo.save();
}
