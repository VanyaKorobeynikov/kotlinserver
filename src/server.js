import 'babel-polyfill'
import express from 'express';
import bodyParser from 'body-parser';
import * as db from './utils/dbutils';
import cors from 'cors';
import multer from 'multer';
import * as fs from 'fs';
import mime from 'mime';
import router from './router';
import AuthCtrl from  './auth';
import {register, login} from './auth';
import passportService from './pass';
import passport from 'passport';
import { imagesPath } from '../../kotlinConfy.js'

const requireAuth = passport.authenticate('jwt', {session: false });
const requireLogin = passport.authenticate('local', { session: false });

const apiRoutes = express.Router(),
        authRoutes = express.Router();

  //=========================
  // Auth Routes
  //=========================

  // Set auth routes as subgroup/middleware to apiRoutes
apiRoutes.use('/auth', authRoutes);




const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, `${imagesPath}/gallery`)
  },
  filename: (req, file, cb) => {
    return cb(null, Date.now()+'.'+mime.extension(file.mimetype))
  }
});
const finishersPhoto = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, `${imagesPath}/finishers`)
  },
  filename: (req, file, cb) => {
    const name = req.body.name.replace(/ /g,"_").toLowerCase();
    return cb(null, name+'.'+mime.extension(file.mimetype))
  }
});
const partnerPhoto = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, `${imagesPath}/partners`)
  },
  filename: (req, file, cb) => {
    const name = req.body.title.replace(/ /g,"_").toLowerCase();
    return cb(null, name+'.'+mime.extension(file.mimetype))
  }
});
const finisherPhotoUpload = multer({ storage: finishersPhoto }).single('photo');
const partnerPhotoUpload = multer({ storage: partnerPhoto }).single('partnerPhoto');
const upload = multer({ storage: storage }).single('photo');

const app = express();

db.setUpConnection();
const urlEncoded = bodyParser.urlencoded({ extended: false });  
const jsonParse = bodyParser.json();
app.use(cors({ origin: '*' }));
app.post('/api/login', urlEncoded, requireLogin, login);
app.get('/test', (req, res) => {
  res.send(db.addMarkers());
});
app.get('/api/finishers', (req, res) => {
  db.getFinishers()
    .then((data) => res.send(data))
    .catch((error) => console.log(error))
});
app.get('/api/finisher/:id', (req, res) => {
  db.getFinisher(req.params.id)
    .then((data) => res.send(data));
});
app.get('/api/finisher/:id/delete', requireAuth, (req, res) => {
  db.deleteFinisher(req.params.id)
    .then((data) => res.send(data));
});

app.get('/api/photos/:year', (req, res) => {
  db.getAllPhotos(req.params.year)
    .then((data) => res.send(data));
  
});
app.get('/api/photo/:id', (req, res) => {
  db.getPhoto(req.params.id)
    .then((data) => res.send(data)); 
});

app.get('/api/partners', (req, res) => {
  db.getAllPartners()
    .then((data) => res.send(data))
    .catch((e) => console.log(e))
  
});
app.get('/api/partner/:id', (req, res) => {
  db.getPartner(req.params.id)
    .then((data) => res.send(data)); 
});

app.get('/api/starts/:type', (req, res) => {
  
  db.getStarts(req.params.type)
    .then((data) => {
      res.send(data)
    })
    .catch((error) => res.send(error))
});

app.get('/api/start/:id', (req, res) => {
  db.getStart(req.params.id)
    .then((data) => res.send(data));
});

app.get('/api/start/:id/delete', requireAuth, (req, res) => {
  db.deleteStart(req.params.id)
    .then((data) => res.send(data));
});
app.get('/api/requests', (req, res) => {
  db.getRequests()
    .then((data) => res.send(data));
});

app.get('/api/request/:id/delete', requireAuth, (req, res) => {
  db.deleteRequest(req.params.id)
    .then((data) => res.send(data));
});

app.post('/api/finisher/add', requireAuth, (req, res) => {
  finisherPhotoUpload(req, res, (err) => {
    const path = req.file ? req.file.filename : '';
    db.addFinisher(req.body, path)
      .then((data) => res.send(data))
      .catch(err => console.log(err))
    if(err) {
        return res.end("Error uploading file.");
    }
  });
});

app.post('/api/finisher/edit', requireAuth, (req, res) => {
  finisherPhotoUpload(req,res,(err) => {
    if(err) {
        return res.end("Error uploading file.");
    }
    db.getFinisher(req.body._id)
      .then((data) => {
        const path = req.file ? req.file.filename : data.photo
        db.editFinisher(req.body, path)
          .then((data) => res.send(data))
          .catch(err => console.log(err))
      })
      .catch(err => console.log(err))
    
  });
});

app.post('/api/start/edit', jsonParse, requireAuth, (req, res) => {
    db.editStart(req.body)
      .then((data) => res.send(data));
});

app.post('/api/photo/add', requireAuth, (req, res) => {
  upload(req,res,(err) => {
    if(err) {
        return console.log("upload error: ", err)
    }
    db.addPhoto(req.body, req.file)
    .then((data) => res.send(data))
    .catch((err) => console.log(err))

  });
});

app.post('/api/partner/add', requireAuth, (req, res) => {
  partnerPhotoUpload(req,res,(err) => {
    db.addPartner(req.body, req.file)
      .then((data) => res.send(data))
      .catch((err) => console.log(err))
    if(err) {
        return console.log("upload error")
    }

  });
});

app.post('/api/start/add', requireAuth, jsonParse, (req, res) => {
    db.addStart(req.body)
      .then((data) => res.send(data))
      .catch(err => console.log(err));
});

app.post('/api/request/add', jsonParse, (req, res) => {
    db.addRequest(req.body)
      .then((data) => res.send(data))
      .catch(err => console.log(err));
});

app.post('/api/photo/edit', jsonParse, requireAuth, (req, res) => {
    db.editPhoto(req.body)
      .then((data) => res.send(data));

});

app.get('/api/photo/:id/delete', requireAuth, (req,res) => {
  db.getPhoto(req.params.id)
    .then((data) => {
      fs.unlink('../kotlinclient/public/images/'+data.name, (err) => {
        if(err) console.log(err);
        db.deletePhoto(data._id)
          .then(() => res.send("Delete complete"))
      })
    .catch((err) => console.log(err))
    })
});
const server = app.listen(3001, function () {
  console.log('Example app listening on port 3001!');
});


