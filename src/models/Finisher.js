import mongoose from 'mongoose';
const Schema = mongoose.Schema;


var FinisherSchema = new Schema({
    name     : { type: String, required: true },
    photo    : { type: Schema.Types.Mixed },
    created  : { type: Date },
    updated  : { type: Date },
    city     : { type: String, default: '' },
    country  : { type: String, default: '' },
    age      : { type: String, default: '' },
    sport    : { type: String, default: '' },
    sex      : { type: String, default: '' },
    about    : { type: String, default: '' },
    starts   : { type: Array, "default": [], ref: 'Start' },
    marker   : { type: Schema.Types.Mixed }
})

const Finisher = mongoose.model('Finisher', FinisherSchema);