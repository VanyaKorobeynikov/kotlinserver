import mongoose from 'mongoose';
const Schema = mongoose.Schema;


var PartnerSchema = new Schema({
    name       : { type: String, required: true },
    annotation : { type: String },
    path       : { type: String, required: true },
    created    : { type: Date },
    updated    : { type: Date },
    link       : { type: String },
    title      : { type: String },
    destination: { type: String },
});

const Partner = mongoose.model('Partner', PartnerSchema);