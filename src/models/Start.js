import mongoose from 'mongoose';
const Schema = mongoose.Schema;


var StartSchema = new Schema({
    name       : { type: String },
    about      : { type: String },
    created    : { type: Date },
    updated    : { type: Date },
    date       : { type: Date },
    time       : { type: String },
    wheather   : { type: String },
    food       : { type: String},  
    type       : { type: String },
    finishers  : [{
        type: Schema.Types.ObjectId, ref: 'Finisher'
    }],
    photos     : [{
        type: Schema.Types.ObjectId, ref: 'Photo'
    }]
});

const Start = mongoose.model('Start', StartSchema);