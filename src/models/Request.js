import mongoose from 'mongoose';
const Schema = mongoose.Schema;


var RequestSchema = new Schema({
    name       : { type: String },
    comment    : { type: String },
    created    : { type: Date },
    updated    : { type: Date },
    country    : { type: String },
    contact    : { type: String },
    age        : { type: String },
    date       : { type: String},  
    type       : { type: String }
});

const Request = mongoose.model('Request', RequestSchema);