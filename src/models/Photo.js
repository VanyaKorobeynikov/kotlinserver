import mongoose from 'mongoose';
const Schema = mongoose.Schema;


var PhotoSchema = new Schema({
    name       : { type: String, required: true },
    path       : { type: String, required: true },
    created    : { type: Date },
    updated    : { type: Date },
    year       : { type: Number },
    finisher   : { type: Schema.Types.ObjectId, ref: 'Finisher' },
    destination: { type: String },
    caption    : { type: String },
    start      : { type: Schema.Types.ObjectId, ref: 'Start' }    
});

const Photo = mongoose.model('Photo', PhotoSchema);