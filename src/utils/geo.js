import axios from 'axios'


export default async function getGeo(city) {
    try {
        if (!city) return null
        const response = await axios.get(`https://geocode-maps.yandex.ru/1.x/?geocode=${city}&kind=locality&format=json`)
        if(response.data.response.GeoObjectCollection.featureMember[0]) {
            const lonlat = response.data.response.GeoObjectCollection.featureMember[0].GeoObject.Point.pos.split(' ');
            return {
                lon: lonlat[0],
                lat: lonlat[1],
            }
        }
    }
    catch (e) {
        console.log(e)
    }
}
