export const getPrettyLetter = ({ name, comment, date, contact, age, country, type }) => (`
<div style="font-family: Arial, Helvetica, sans-serif; font-size: 24px;">
    <div style="width: 100%; padding: 16px; margin-bottom: 24px; background: #151515; color: #fff; border-bottom: 8px solid #c52d2f">
        <p>
            <b>Привет!</b> 
            Новая заявка на участие в KotlinRace!!!
        </p>
    </div>
    <div style="padding: 16px;">
        <p>Данные:</p>
        <p><b>Имя: </b>${name}</p>
        <p><b>Контакты: </b>${contact}</p>
        <p><b>Дата: </b>${date}</p>
        <p><b>Возраст: </b>${age}</p>
        <p><b>Страна: </b>${country}</p>
        <p><b>Тип гонки: </b>${type}</p>
        <p><b>Комментарий: </b>${comment}</p>
    </div>
</div>
`)
