import mongoose from 'mongoose';
import { get } from 'lodash'
import '../models/Finisher';
import '../models/Photo';
import '../models/Start';
import '../models/Request';
import '../models/Partner';
import { getPrettyLetter } from './mail.js'
import { userName, password, dbName, adminEmail } from './confy.js'
import nodemailer from 'nodemailer'
import getGeo from './geo.js'
import axios from 'axios'

const Finisher = mongoose.model('Finisher');
const Photo = mongoose.model('Photo');
const Start = mongoose.model('Start');
const Request = mongoose.model('Request');
const Partner = mongoose.model('Partner');


export function setUpConnection() {
    const dbAdress = (userName && password)
        ? `mongodb://${userName}:${password}@localhost/${dbName}`
        : `mongodb://@localhost/${dbName}`
    mongoose.connect(dbAdress, {
        //mongoose.connect(`mongodb://${userName}:${password}@localhost/${dbName}`, {
        useMongoClient: true,
    });
}

export async function addFinisher(data, path) {
    let marker
    const city = get(data, 'city', null)
    try {
        marker = await getGeo(city)
    }
    catch(e) {
        console.log(e)
    }

    const finisher = new Finisher({
        name: data.name,
        city: data.city,
        photo: path,
        country: data.country,
        age: data.age,
        sex: data.sex,
        about: data.about,
        sport: data.sport,
        created: Date(),
        updated: Date(),
        starts: [],
        marker: marker || null,
    });
    return finisher.save();
}

export async function editFinisher(data, path) {
    let marker
    const city = get(data, 'city', null)
    try {
        marker = await getGeo(city)
    }
    catch(e) {
        console.log(e)
    }

    return Finisher.update({ _id: data._id }, {
        name: get(data, 'name', ''),
        city: get(data, 'city', ''),
        country: get(data, 'country', ''),
        photo: path,
        age: data.age,
        sex: get(data, 'sex', ''),
        about: get(data, 'about', ''),
        marker: marker || null,
        sport: get(data, 'sport', ''),
        updated: Date(),
    })

}

export function editStart(data) {
    return Start.update({ _id: data._id }, {
        name: data.name,
        about: data.about,
        updated: Date(),
        date: data.date,
        time: data.time,
        wheather: data.wheather,
        food: data.food,
        type: data.type,
        finishers: data.finishers
    });



}

export function getFinishers() {
    return Finisher.find();
}
export function getAllPartners() {
    return Partner.find();
}
export function getStarts(type) {
    if (type == "undefined")
        return Start.find()
            .populate('finishers')
            .sort({ date: -1 })
            .exec((err, starts) => {
                if (err) console.log(err);
                return starts
            });
    if (type == "solo")
        return Start.find()
            .populate('finishers')
            .sort({ date: -1 })
            .where({ type: "solo" })
            .exec((err, starts) => {
                if (err) console.log(err);
                return starts
            });
    if (type == "relay")
        return Start.find()
            .populate('finishers')
            .sort({ date: 1 })
            .where({ type: "relay" })
            .exec((err, starts) => {
                if (err) console.log(err);
                return starts
            });
}
export function getRequests() {
    return Request.find();
}

export function getFinisher(id) {
    return Finisher.findOne({ _id: id });
}
export function getPartner(id) {
    return Partner.findOne({ _id: id });
}

export function getStart(id) {
    return Start.findOne({ _id: id })
        .populate('photos')
        .populate('finishers')
        .exec((err, start) => {
            if (err) console.log(err);
            return start;
        });
}
export function deleteStart(id) {
    return Start.remove({ _id: id }, err => {
        if (err) return console.log(err);
        return "removed";
    })
}
export function deleteRequest(id) {
    return Request.remove({ _id: id }, err => {
        if (err) return console.log(err);
        return "removed";
    })
}
export function deleteFinisher(id) {
    return Finisher.remove({ _id: id }, err => {
        if (err) return console.log(err);
        return "removed";
    })
}
export function deletePhoto(id) {
    return Photo.remove({ _id: id }, err => {
        if (err) return console.log(err);
        return "removed";
    })
}
export function addStart(data) {
    const start = new Start({
        name: data.name,
        about: data.about,
        created: Date(),
        updated: Date(),
        date: data.date,
        time: data.time,
        wheather: data.wheather,
        food: data.food,
        type: data.type,
        finishers: data.finishers
    });


    return start.save();

}

export function addRequest(data) {
    const transporter = nodemailer.createTransport({
        host: 'localhost',
        port: 25,
        secure: false,
        tls: {
            rejectUnauthorized: false
        }
    })
    var mailOptions = {
        from: '"KotlinRace" <vanya@kotlinrace.ru>', // sender address
        to: adminEmail, // list of receivers
        subject: 'Новый запрос на регистрацию на KotlinRace', // Subject line
        text: 'Hello world ?', // plaintext body
        html: getPrettyLetter(data) // html body
    };

    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log(error);
        }
    })
    const request = new Request({
        name: data.name,
        comment: data.comment,
        created: Date(),
        updated: Date(),
        date: data.date,
        contact: data.contact,
        country: data.country,
        age: data.age,
        type: data.type
    });

    return request.save();

}

export function editPhoto(data) {
    /* Photo.findByIdAndUpdate(data._id, (err, photo) => {
         if(err) console.log(err);
         photo.updated = Date();
         photo.year = data.year;
         photo.caption = data.caption;
         if(data.finisher && data.finisher != "undefined" && data.finisher != "noselect"){
             photo.set({finisher: data.finisher})
         }else{
             photo.unset({finisher: 1})
         }
     return photo.save();
     })*/
    /*Photo.find({}, function(err, photos){
        photos.forEach(function(element) {
        Start.update({_id: element.start}, {
            $push: {
                photos: element._id
            }
        }).exec();
    }, this);
    })
   
    
    return 'ok';*/
    /*Photo.findByIdAndUpdate(data._id, (err, photo) => {
        if(err) console.log(err);
        photo.updated = Date();
        photo.year = data.year;
        photo.caption = data.caption;
        if(data.finisher && data.finisher != "undefined" && data.finisher != "noselect"){
            photo.set({finisher: data.finisher})
        }else{
            photo.unset({finisher: 1})
        }
    return photo.save();
    })*/
    return Photo.update({ _id: data._id },
        {
            $set: {
                updated: Date(),
                year: data.year,
                caption: data.caption
            }
        });
}


export function getAllPhotos(year) {
    if (year < 2000) {
        return Photo
            .find()
            .populate('finisher')
            .populate('start')
            .exec((err, photo) => {
                if (err) console.log(err);
                return photo;
            });
    } else {
        return Photo.find({ year: year })
    }
}

export function getPhoto(id) {
    return Photo.findOne({ _id: id }).populate('finisher').exec((err, photo) => {
        return photo;
    });
}

export function addMarkers() {
    const finishers = Finisher.find()
        .exec((err, finisher) => {
            finisher.forEach((item) => {
                getGeo(item)
            })
        })
    return 'ok'
}

export function addPartner(data, file) {
    const partner = new Partner({
        name: file.filename,
        path: file.path,
        created: Date(),
        updated: Date(),
        link: data.link,
        title: data.title,
        destination: file.destination,
        annotation: data.annotation,
    });
    return partner.save();
}

export function addPhoto(data, file) {

    return Photo.create({
        name: file.filename,
        path: file.path,
        created: Date(),
        updated: Date(),
        year: data.year,
        destination: file.destination,
        caption: data.caption,
    }, (err, photo) => {
        Start.update({ _id: data.start }, {
            $push: {
                photos: photo._id
            }
        }).exec();
        if (data.finisher && data.finisher != 'noselect' && data.finisher != 'undefined') {
            Photo.update({ _id: photo._id }, { $set: { finisher: data.finisher } }).exec();
        }
        if (data.start && data.start != 'noselect' && data.start != 'undefined') {
            Photo.update({ _id: photo._id }, { start: data.start }).exec();
        }
    })
    /*
    const photo = new Photo({
        name     : file.filename,
        path     : file.path,
        created  : Date(),
        updated  : Date(),
        year     : data.year,
//        finisher : data.finisher || data.finisher == 'select finisher' ? data.finisher : null,
        destination: file.destination,
        caption  : data.caption,
//        start    : data.start    
    });*/


    //return photo.save();
}
