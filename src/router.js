import passportService from './pass';
import express from 'express';
import passport from 'passport';
import {register, login} from './auth';

const requireAuth = passport.authenticate('jwt', {session: false });
const requireLogin = passport.authenticate('local', { session: false });

export default function(app) {  
  // Initializing route groups
  const apiRoutes = express.Router(),
        authRoutes = express.Router();

  //=========================
  // Auth Routes
  //=========================

  // Set auth routes as subgroup/middleware to apiRoutes
  apiRoutes.use('/auth', authRoutes);

  // Registration route
  authRoutes.post('/register', register);

  // Login route
  authRoutes.post('/login', requireLogin, login);

// Set url for API group routes
  app.use('/api', apiRoutes);
};